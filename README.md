# OpenVPN Admin


[![pipeline status](https://gitlab.com/mateusdemboski/OpenVPN.Admin/badges/develop/pipeline.svg)](https://gitlab.com/mateusdemboski/OpenVPN.Admin/commits/develop)

## Use of
- [Tabler](https://github.com/tabler/tabler)
- [.NET Core](https://github.com/dotnet/coreclr)
- [ASP.NET Core](https://github.com/aspnet/Home)
- [Entity Framework](https://github.com/aspnet/EntityFrameworkCore)
- [SQLite](https://www.sqlite.org/index.html)
