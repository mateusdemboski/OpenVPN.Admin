using System;

namespace OpenVPN.Admin.ServerParser.Exceptions
{
    [System.Serializable]
    public class ParserException : System.Exception
    {
        public ParserException(int lineNumber, string message) 
            : base($"Parsing error on line '{lineNumber}': {message??"Invalid token"}") {}

        public ParserException(int lineNumber) : this(lineNumber, null) { }
    }
}