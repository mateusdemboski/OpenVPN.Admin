using OpenVPN.Admin.ServerParser.Types;

namespace OpenVPN.Admin.ServerParser.Models
{
    public class StatusToken
    {
        public StatusTokenType Type { get; set; }
        public string Value { get; set; }
    }
}