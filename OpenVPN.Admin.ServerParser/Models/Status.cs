using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;

namespace OpenVPN.Admin.ServerParser.Models
{
    public class Status
    {
        public IEnumerable<Client> ConnectedClients { get; set; }
        public IEnumerable<Routing> RoutingTable { get; set; }
        public string ServerStatus { get; set; }
        public DateTime UpdatedAt { get; set; }

        [Description("Max bcast/mcast queue length")]
        public int MaxBcastMcastQueueLength { get; set; }

        public class Client
        {
            [Description("Common Name")]
            public string CommonName { get; set; }

            [Description("Real Address")]
            public IPEndPoint RealAddress { get; set; }

            [Description("Bytes Received")]
            public long BytesReceived { get; set; }

            [Description("Bytes Sent")]
            public long BytesSent { get; set; }

            [Description("Connected Since")]
            public DateTime ConnectedSince { get; set; }
        }

        public class Routing 
        {
            [Description("Virtual Address")]
            public IPAddress VirtualAddress { get; set; }

            [Description("Common Name")]
            public string CommonName { get; set; }

            [Description("Real Address")]
            public IPEndPoint RealAddress { get; set; }
            
            [Description("Last Ref")]
            public DateTime LastRef { get; set; }
        }
        
    }
}