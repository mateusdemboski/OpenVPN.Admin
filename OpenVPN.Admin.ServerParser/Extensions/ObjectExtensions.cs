using System.ComponentModel;
using System.Reflection;

namespace OpenVPN.Admin.ServerParser.Extensions
{
    public static class ObjectExtensions
    {
        public static string GetPropertyDescription<T>(this T obj, string propertyName) where T : class
        {
            string result = null;
            PropertyInfo property = typeof(T).GetProperty(propertyName);
            if (property != null)
            {
                try
                {
                    object[] descriptionAttrs = property.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    DescriptionAttribute description = (DescriptionAttribute)descriptionAttrs[0];
                    result = description.Description;
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public static PropertyInfo GetPropertyByDescription<T>(this T obj, string propertyDescription) where T : class
        {
            var type = typeof(T);

            foreach (var property in type.GetProperties())
            {
                if(obj.GetPropertyDescription(property.Name) == propertyDescription)
                    return property;
            }

            return null;
        }
    }
}