using System.ComponentModel;

namespace OpenVPN.Admin.ServerParser.Types
{
    public enum StatusSectionType
    {
        [Description("OpenVPN CLIENT LIST")]
        ClientList,
        [Description("ROUTING TABLE")]
        RoutingTable,
        [Description("GLOBAL STATS")]
        GlobalStats,
        [Description("END")]
        End
    }
}