namespace OpenVPN.Admin.ServerParser.Types
{
    public enum StatusTokenType
    {
        SECTION,
        HEADER,
        SECTIONITEM,
        TUPLE,
        EOF
    }
}