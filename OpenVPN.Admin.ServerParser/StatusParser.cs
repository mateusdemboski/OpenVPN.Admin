﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using OpenVPN.Admin.ServerParser.Exceptions;
using OpenVPN.Admin.ServerParser.Extensions;
using OpenVPN.Admin.ServerParser.Models;
using OpenVPN.Admin.ServerParser.Types;

namespace OpenVPN.Admin.ServerParser
{
    public class StatusParser : IParser<Status>
    {
		private const char ItemSeparator = ',';
		private const string DefaultDateTimeFormat = "ddd MMM d HH:mm:ss yyyy";
		private readonly string[] Log;
		private int Pos = 0;
		private string Currentline;
		private StatusToken CurrentToken;

		public StatusParser(string[] log)
		{
			this.Log = log;
			this.Currentline = this.Log[this.Pos];
		}

		public StatusParser(string path)
		{
			if (!File.Exists(path))
				throw new ArgumentException("The especified path does not exists!", nameof(path));
			
			var log = File.ReadAllLines(path);

			this.Log = log;
			this.Currentline = this.Log[this.Pos];
		}

		#region Lexer code
		/// Advance the 'Pos' pointer and set the 'CurrentLine' variable.
		private void Advance()
		{
			this.Pos++;
			if(this.Pos > this.Log.Length - 1)
				this.Currentline = null; // Indicates end of input
			else
				this.Currentline = this.Log[this.Pos];
		}

		/// Verify if line is a Section
		private bool IsSection(string line, bool validateEOF = false)
        {
            var type = typeof(StatusSectionType);
            foreach (Enum section in Enum.GetValues(type))
            {
                var field = type.GetField(section.ToString());
                var sectionDescription = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                foreach (DescriptionAttribute description in sectionDescription)
                {
                    if(description.Description == line && !validateEOF)
                        return true;
					else if(description.Description == line && validateEOF)
						return (StatusSectionType)section == StatusSectionType.End;
                }
            }

            return false;
        }

		/// Verify if line is a Header
		private bool IsHeader(string line)
		{
			var regexValidator = new Regex(@"^[a-zA-Z\s,]+$");
			return regexValidator.IsMatch(line);
		}

		/// Verify if line is a Section Item
		private bool IsSectionItem(string line)
		{
			var commonNameExp = "[A-Za-z0-9]+";
			var addressExp = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
			var realAddressExp = $"{addressExp}:[0-9]+";
			var numbersOnlyExp = "[0-9]+";
			var dateExp = @"[A-za-z0-9\:\s]+";
			var clientListExp = new Regex($"({commonNameExp}),({realAddressExp}),({numbersOnlyExp}),({numbersOnlyExp}),({dateExp})");
			var routingTableExp = new Regex($"({addressExp}),({commonNameExp}),({realAddressExp}),({dateExp})");
			
			return clientListExp.IsMatch(line) || routingTableExp.IsMatch(line);
		}

		private bool IsKeyValueItem(string line)
		{
			if(String.IsNullOrEmpty(line)) return false;

			var splited = line.Split(ItemSeparator);

			return splited.Length == 2;
		}

		/// Lexical analyzer (also known as scanner or tokenizer)
        /// <summary>
        /// This method is responsible for breaking a line
        /// apart into tokens. One token at a time.
        /// </summary>
		private StatusToken GetNextToken()
		{	
			//Check if is EOF
			if(IsSection(this.Currentline, true))
				return new StatusToken() { Type = StatusTokenType.EOF, Value = null };

			else if(IsKeyValueItem(this.Currentline))
				return new StatusToken() { Type = StatusTokenType.TUPLE, Value = this.Currentline };

			else if(IsSection(this.Currentline))
				return new StatusToken() { Type = StatusTokenType.SECTION, Value = this.Currentline };
			
			else if(IsHeader(this.Currentline))
				return new StatusToken() { Type = StatusTokenType.HEADER, Value = this.Currentline };
			
			else if(IsSectionItem(this.Currentline))
				return new StatusToken() { Type = StatusTokenType.SECTIONITEM, Value = this.Currentline };
			
			throw new ParserException(this.Pos + 1,"Syntax error");
		}
		
		#endregion

        #region Parser / Interpreter code

		/// <summary>
        /// compare the current token type with the passed token
        /// type and if they match then "eat" the current token
        /// and assign the next token to the this.CurrentToken,
        /// otherwise throw an exception.
        /// </summary>
		private void Eat(StatusTokenType type)
		{
			if(this.CurrentToken.Type == type)
			{
				this.Advance();
				if(this.CurrentToken.Type != StatusTokenType.EOF)
					this.CurrentToken = this.GetNextToken();
			}
			else
				throw new ParserException(Pos + 1, $"Unxpected token ({this.CurrentToken.Type.ToString()})'{this.CurrentToken.Value}', expected '{type.ToString()}'");		}

		/// Parser / Interpreter
		public Status Parse()
		{
			// set current token to the first token taken from the input
			this.CurrentToken = this.GetNextToken();

			var parsedResult = new Status();
			var connectedClients = new List<Status.Client>();
			var routingTable = new List<Status.Routing>();
			
			this.Eat(StatusTokenType.SECTION);
			var currentStatus = this.CurrentToken;
			this.Eat(StatusTokenType.TUPLE);
			var connectedClientsHeader = this.CurrentToken;
			this.Eat(StatusTokenType.HEADER);
			while(this.CurrentToken.Type == StatusTokenType.SECTIONITEM)
			{
				var sectionItem = this.CurrentToken;
				this.Eat(StatusTokenType.SECTIONITEM);
				connectedClients.Add(this.ParseTokenToModel<Status.Client>(connectedClientsHeader, sectionItem));
			}
			this.Eat(StatusTokenType.SECTION);
			var routingTableHeader = this.CurrentToken;
			this.Eat(StatusTokenType.HEADER);
			while(this.CurrentToken.Type == StatusTokenType.SECTIONITEM)
			{
				var sectionItem = this.CurrentToken;
				this.Eat(StatusTokenType.SECTIONITEM);
				routingTable.Add(this.ParseTokenToModel<Status.Routing>(routingTableHeader, sectionItem));
			}
			this.Eat(StatusTokenType.SECTION);
			var mbmqlLine = this.CurrentToken;
			this.Eat(StatusTokenType.TUPLE);
			this.Eat(StatusTokenType.EOF);

			var serverStatus = currentStatus.Value.Split(ItemSeparator);
			var maxBcastMcastQueueLength = mbmqlLine.Value.Split(ItemSeparator)[1];
			parsedResult.ServerStatus = serverStatus[0];
			parsedResult.UpdatedAt = ConvertStringToDateTime(serverStatus[1]);
			parsedResult.ConnectedClients = connectedClients.Count > 0 ? connectedClients : null;
			parsedResult.RoutingTable = routingTable.Count > 0 ? routingTable : null;
			parsedResult.MaxBcastMcastQueueLength = Int32.Parse(maxBcastMcastQueueLength);

			return parsedResult;
		}

		#endregion

		#region Reflection

		private T ParseTokenToModel<T>(StatusToken headerToken, StatusToken sectionItemToken) where T : class, new()
		{
			var result = new T();
			var headerMap = headerToken.Value.Split(ItemSeparator);
			var sectionItemMap = sectionItemToken.Value.Split(ItemSeparator);

			if(headerMap.Length != sectionItemMap.Length)
				throw new ArgumentException($"The parameter {nameof(headerToken)} don't appears be a header for {nameof(sectionItemToken)}, because {nameof(headerToken)} have {headerMap.Length} items and {nameof(sectionItemToken)} have {sectionItemMap.Length} items");

			for (int i = 0; i < headerMap.Length; i++)
			{
				var header = headerMap[i];
				var property = result.GetPropertyByDescription(header);
				if(property == null) continue;
				else if(property.PropertyType == typeof(IPAddress))
				property.SetValue(result, ConvertStringToIPAddress(sectionItemMap[i]));
				else if(property.PropertyType == typeof(IPEndPoint))
					property.SetValue(result, ConvertStringToIPEndPoint(sectionItemMap[i]));
				else if(property.PropertyType == typeof(DateTime))
					property.SetValue(result, ConvertStringToDateTime(sectionItemMap[i]));
				else	
					property.SetValue(result, Convert.ChangeType(sectionItemMap[i], property.PropertyType));
			}

			return result;
		}

		private IPAddress ConvertStringToIPAddress(string ipAddress)
		{
			IPAddress ip;
			if(!IPAddress.TryParse(ipAddress, out ip))
			{
				throw new FormatException("Invalid ip-adress");
			}

			return ip;
		}
		private IPEndPoint ConvertStringToIPEndPoint(string endPoint)
		{
			string[] ep = endPoint.Split(':');
			if(ep.Length != 2) throw new FormatException("Invalid endpoint format");
			
			var ip = ConvertStringToIPAddress(ep[0]);

			int port;
			if(!int.TryParse(ep[1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out port))
			{
				throw new FormatException("Invalid port");
			}
			return new IPEndPoint(ip, port);
		}

		private DateTime ConvertStringToDateTime(string date)
		{
			return DateTime.ParseExact(date, DefaultDateTimeFormat, CultureInfo.InvariantCulture);
		}

		#endregion
    }
}
