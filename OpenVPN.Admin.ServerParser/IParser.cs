namespace OpenVPN.Admin.ServerParser
{
    public interface IParser
    {
        void Parse();
    }

    public interface IParser<ParsedResultType>
    {
        ParsedResultType Parse();
    }
}