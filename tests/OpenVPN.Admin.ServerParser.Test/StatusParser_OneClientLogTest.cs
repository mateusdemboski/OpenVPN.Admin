using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenVPN.Admin.ServerParser.Models;

namespace OpenVPN.Admin.ServerParser.Test
{
    [TestClass]
    public class StatusParser_OneClientLogTest
    {

        private void TestStatusClassForOneClientLogFile(Status status)
        {
            Assert.IsNotNull(status);
            Assert.IsNotNull(status.ConnectedClients);
            Assert.AreEqual(status.ConnectedClients.Count(), 1);
            Assert.IsNotNull(status.RoutingTable);
            Assert.AreEqual(status.RoutingTable.Count(), 1);
            Assert.AreEqual(status.MaxBcastMcastQueueLength, 1);
        }

        [TestMethod]
        public void TestEmptyClientLogFilePassingPath()
        {
            var parser = new StatusParser(Path.Combine("..","..","..","ValidStatusLogs", "oneClient.log"));
            var status = parser.Parse();

            TestStatusClassForOneClientLogFile(status);
        }


        [TestMethod]
        public void TestOneClientLogFilePassingLogContent()
        {
            var log = File.ReadAllLines(Path.Combine("..","..","..","ValidStatusLogs", "oneClient.log"));
            var parser = new StatusParser(log);
            var status = parser.Parse();

            TestStatusClassForOneClientLogFile(status);
        }
    }
}
