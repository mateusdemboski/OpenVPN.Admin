using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenVPN.Admin.ServerParser.Models;

namespace OpenVPN.Admin.ServerParser.Test
{
    [TestClass]
    public class StatusParser_EmptyClientLogTest
    {

        private void TestStatusClassForEmpryLogFile(Status status)
        {
            Assert.IsNotNull(status);
            Assert.IsNull(status.ConnectedClients);
            Assert.IsNull(status.RoutingTable);
            Assert.AreEqual(status.MaxBcastMcastQueueLength, 1);
        }

        [TestMethod]
        public void TestEmptyClientLogFilePassingPath()
        {
            var parser = new StatusParser(Path.Combine("..","..","..","ValidStatusLogs", "empty.log"));
            var status = parser.Parse();

            TestStatusClassForEmpryLogFile(status);
        }


        [TestMethod]
        public void TestEmptyClientLogFilePassingLogContent()
        {
            var log = File.ReadAllLines(Path.Combine("..","..","..","ValidStatusLogs", "empty.log"));
            var parser = new StatusParser(log);
            var status = parser.Parse();

            TestStatusClassForEmpryLogFile(status);
        }
    }
}
